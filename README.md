# Paytime - Backend N1

Primeiramente, obrigado pelo seu interesse em trabalhar conosco.

A seguir você encontrará todos as informações necessárias para fazer seu teste.

## Estudo de Caso

Contratamos os seus serviços para desenvolver uma aplicação que será responsável por executar transferências de valores entre contas.

A aplicação se baseia em um gerenciamento de uma ou mais contas bancárias, sendo elas Físicas e/ou Jurídicas.

Na versão 1.0 BETA do nosso sistema, precisamos apenas que as contas sejam identificadas via CPF ou CNPJ, outros dados são opcionais.

Como queremos agradar muito os nossos clientes, logo, pensamos em disponibilizar mais de uma conta para cada.

O cliente poderá efetuar uma recarga de um valor qualquer para a sua conta de preferência.

O objetivo principal é a transferência de valores entre as contas dos clientes.

## Requisitos Funcionais

- Seu serviço deve:
    - ser acessível através de uma API REST
    - conter um gerenciamento de usuários e contas
    - conter um gerenciamento de saldo das contas

## Requisitos não funcionais

- Seu serviço deve ser construído com atenção aos seguintes aspectos:
    - Organização
    - Padronização
    - Tratamento de Erros
    - Disponibilidade
    - Persistência
    - Manutenibilidade
    - Segurança

- Será um diferencial se você apresentar uma **documentação** que represente a arquitetura de seu serviço e, se necessário, uma explicação para suas decisões de arquitetura.


## Detalhes

Você pode utilizar qualquer linguagem, ferramenta, framework ou biblioteca JavaScript.

Caso tenha quaisquer dúvidas sobre este teste, a qualquer momento, sinta-se à vontade para entrar em contato através do e-mail rh@paytime.com.br.

Commite suas alterações de forma organizada.

Você deve criar um fork deste repositorio, ao terminar todo o desafio você devera abrir um **Pull Request**.

No README deve constar como instalar e executar a aplicação.

## Dicas importantes:

- Por uma questão de aderência a posição, a utilização de TypeScript, Framework AdonisJS ou NestJS para qual está descrita esta vaga será um diferencial.

- Lembre-se que o usuário só pode ter um CPF e/ou ser dono de mais de um CNPJ.

- Não se esqueça efetuar o Pull Request e nos avisar que você terminou o desafio (rh@paytime.com.br).

- Boa Sorte.